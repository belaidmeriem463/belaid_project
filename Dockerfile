# Utiliser l'image Nginx 
FROM nginx:latest

# Copier le fichier HTML dans le répertoire racine du serveur web
COPY index.html /usr/share/nginx/html/index.html
